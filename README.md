# Demo project for Drupal8+ReactJS

See [this presentation](https://docs.google.com/presentation/d/1-XsgAjBjDs-ITNMBXVt23wzU6ByN68IbpVUAahFwjEs/edit?usp=sharing)

Run these (you need to know what you're doing)

```
git clone ...
cd d8-react
composer install
# ... configure and setup drupal
cd web/app
npm install

```
